<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ScriptController extends Controller
{
    public function review(Request $request)
    {
        $this->validate($request, [
            'introducion' => 'required',
            'topic1'      => 'required',
            'topic2'      => 'required'
        ]);

        $input = $request->all();
        $request->session()->put('introducion', $input['introducion']);
        $request->session()->put('topic1', $input['topic1']);
        $request->session()->put('topic2', $input['topic2']);
        return view('review-script', ['input' => $input]);
    }

    public function prepare(Request $request)
    {
        //   $input = $request->all();
        $input['introducion'] = $request->session()->get('introducion','');
        $input['topic1'] = $request->session()->get('topic1','');
        $input['topic2'] = $request->session()->get('topic2','');

        return view('prepare', ['input' => $input]);
    }
    public function getPrepare(Request $request)
    {
        //   $input = $request->all();
        $input['introducion'] = $request->session()->get('introducion','');
        $input['topic1'] = $request->session()->get('topic1','');
        $input['topic2'] = $request->session()->get('topic2','');

        return view('prepare', ['input' => $input]);
    }
    public function record(Request $request)
    {
        //  $input = $request->all();
        $input['introducion'] = $request->session()->get('introducion','');
        $input['topic1'] = $request->session()->get('topic1','');
        $input['topic2'] = $request->session()->get('topic2','');

        return view('record', ['input' => $input]);
    }
    public function getRecord(Request $request)
    {
        //  $input = $request->all();
        $input['introducion'] = $request->session()->get('introducion','');
        $input['topic1'] = $request->session()->get('topic1','');
        $input['topic2'] = $request->session()->get('topic2','');

        return view('record', ['input' => $input]);
    }

    public function getPreview()
    {
        $previews = array();
        $files = $this->getPreviews();
        foreach ($files as $link) {
            $previews[] = $link . '_qvga_thumb.png';
        }
        return response()->json($previews);

    }

    public function selectVideo()
    {
        $files = $this->getPreviews();
        $previews = array();
        $i = 0;
        foreach ($files as $link) {
            $previews[$i]['link'] = $link . '_qvga_thumb.png';
            $previews[$i]['name'] = $link;
            $i++;
        }
        return view('select-video', ['previews' => $previews]);

    }

    private function getPreviews()
    {
        $url = 'http://139.59.130.84/a-9a5e5660-75a3-0134-c71d-1217e1551dfd/';
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $out = curl_exec($curl);
        }
        preg_match_all('/<a href="([a-z\-0-9\_]+)_qvga_thumb.png">/is', $out, $matches);
        return $matches[1];
    }

    public function editVideo(Request $request)
    {
        $input = $request->all();
        if (isset($input['file'])) {
            $param = array('name' => $input['file']);
            //    $fileName = $input['file'];
            if ($curl = curl_init()) {
                curl_setopt($curl, CURLOPT_URL, 'http://139.59.130.84/edit_video');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($param));
                $out = curl_exec($curl);
                //     echo $out;
                curl_close($curl);
            }
        }
        $response = json_decode($out);

        /*    if(!isset($response['poster'])){
                $response['poster']='';
            }*/
        //       exit();
        //    $url=
        return view('edit', ['input' => $input, 'response' => $response]);
    }

}

