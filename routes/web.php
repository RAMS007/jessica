<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => ['web']], function () {



    Route::get('/', function () {
        return view('layout.mainPage');
    });

    Route::get('/get-started', function () {
        return view('get-started');
    });

    Route::get('/prepare','ScriptController@getPrepare');
    Route::post('/prepare', 'ScriptController@prepare');

    Route::get('/review-script', function () {
        return view('review-script');
    });
    Route::post('/review-script', 'ScriptController@review');

    Route::get('/record',  'ScriptController@getRecord');
    Route::post('/record', 'ScriptController@record');

    Route::any('/select-video', 'ScriptController@selectVideo');

    Route::any('/edit','ScriptController@editVideo');
    Route::get('/final-page-review', function () {
        return view('final-page-review');
    });
    Route::get('/video-resumes', function () {
        return view('video-resumes');
    });
    Route::get('/our-process', function () {
        return view('our-process');
    });
    Route::get('/our-team-1', function () {
        return view('our-team-1');
    });
    Route::get('/clients', function () {
        return view('clients');
    });
    Route::get('/support', function () {
        return view('support');
    });

    Route::get('/preview', 'ScriptController@getPreview');






});







