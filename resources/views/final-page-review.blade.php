@extends('layout.baseBootstrap')
@section('content')
    <div class="wrapper">
        <h4>Jessica Sarkisian</h4>

        <div class="row">
            <div class="col-sm-8">
                <div class="video">
                    <iframe allowfullscreen=""
                            src="//www.youtube.com/embed/Hsux3WzaQrs?wmode=transparent&amp;autoplay=0&amp;theme=dark&amp;controls=1&amp;autohide=0&amp;loop=0&amp;showinfo=0&amp;rel=0&amp;playlist=false&amp;enablejsapi=0"
                            width="620" height="420" ></iframe>
                </div>
            </div>

            <div class="col-sm-4">
                <p>Contact and other information. (etc.)</p>


                <form>
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter your email">
                    </div>
                    <div class="form-group">
                        <label for="year">Year Graduated:</label>
                        <input type="year" class="form-control" id="year" placeholder="Year Graduated">
                    </div>
                    <div class="form-group">
                        <label for="address">Address:</label>
                        <input type="address" class="form-control" id="year" placeholder="Enter your address">
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>


            </div>

        </div>
        <div class="redButton center inline">
            <a href="" target="_self">
                Download </a>
        </div>
    </div>

@endsection