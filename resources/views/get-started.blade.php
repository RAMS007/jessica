@extends('layout.baseBootstrap')
@section('content')
    <h4 class="font_4">Starting Your Rezzio</h4>
    <div class="video  center">
        <iframe allowfullscreen=""
                src="//www.youtube.com/embed/PMWmYf2043g?wmode=transparent&amp;autoplay=0&amp;theme=dark&amp;controls=1&amp;autohide=0&amp;loop=0&amp;showinfo=0&amp;rel=0&amp;playlist=false&amp;enablejsapi=0"
                width="640" height="480"></iframe>
    </div>

    <div class="redButton center">
        <a href="{{url('/support')}}" target="_self">
            I am confused-- Contact Me</a>
    </div>

    <div class="redButton center">
        <a href="{{url('/prepare')}}" target="_self">
            I am ready-- LETS GO!</a>
    </div>
@endsection
