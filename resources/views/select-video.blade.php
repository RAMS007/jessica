@extends('layout.baseBootstrap')

@section('content')
    <div class="wrapper">
        <h1>Step 3: Select attempt</h1>

        <div id="attempts">
            @foreach($previews as $preview)
                <div class="imageAttempts"><img
                            src="/a-9a5e5660-75a3-0134-c71d-1217e1551dfd/{{$preview['link']}}">

                    <form method="POST" action="{{url('/edit')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="file" value="{{$preview['name']}}">
                        <input type="submit" class="redButton center"
                               value="Select Attempt">
                    </form>
                </div>
            @endforeach
        </div>
        <div class="clearfix"></div>
        <!--        <div id="fiu4uinlineContent" class="s7inlineContent">

                        <div style="left: 43px; width: 304px; position: absolute; top: 40px;" class="s2"
                             id="comp-iu5ma0oz3"><h6 class="font_6">Video Attempts</h6>
                        </div>
                        <div id="comp-iu5maqph" data-align="center"
                             style="left: 432px; position: absolute; top: 350px; height: 40px; min-height: 28px; ">
                            <form method="POST" action="{{url('/record')}}" style="display: inline-block;">
                                {{csrf_field()}}
                <input type="submit" id="comp-iu5l74iwlabel" class="buttonsred" value="Re Record">
            </form>
        </div>
    </div>-->

    </div>
@endsection