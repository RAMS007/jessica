@extends('layout.baseBootstrap')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/src/css/custom.css">
    <link rel="stylesheet" type="text/css" href="/dist/demo.css">

    <h1>Step 4: Editing</h1>

    <div class="container content-block">
        <div class="row">
            <h4 class="center">ImgToVideo module</h4>
        </div>
        <div class="row">
            <div class="item-center" id='root'></div>
        </div>
    </div>
    <div class="container instruction-block">
        <div class="row ">
            <h4 class="center">To inject an image</h4>

            <div class="">
                <div class="twelve columns">
                    <li>Pause a video at any timeframe</li>
                    <li>Then, choose a duration (in seconds)</li>
                    <li>Next, click "Choose File" to upload an image to the server</li>
                    <li>Press Render to initiate rendering process and wait for a modal</li>
                    <li>The resulting videos are stored at /results folder</li>
                </div>
            </div>

        </div>
    </div>
    <div class="center">
        <div class="redButton center inline">
            <a href="{{url('/select-video')}}" target="_self">
                Go Back</a>
        </div>

        <div class="redButton center inline">
            <a href="{{url('/record')}}" target="_self">
                Re Record</a>
        </div>
        <div class="redButton center inline">
            <a href="{{url('/final-page-review')}}" target="_self">
                Move On</a>
        </div>


    </div>

    <script src="http://139.59.130.84/dist/react-html5-video-editor.js"></script>
    <script src="http://139.59.130.84/static/react-html5-video-editor.js"></script>
    <script type="text/javascript">
        ReactHtml5VideoEditor.render_editor("{{$response->poster or '' }}", "{{$response->vid_path or '' }}");
    </script>
@endsection