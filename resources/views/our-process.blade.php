@extends('layout.baseBootstrap')
@section('content')

    <div id="backgroundProcess">
        <h2>OUR PROCESS </h2>
    </div>
    <div class="wrapper">
        <div>
            <h5><span>Need more details? Contact us</span>
            </h5>

            <p><span>We are here to assist. Contact us by phone, email or via our Social Media channels.</span>
            </p>

            <div>
                <a href="{{url('/support')}}" target="_self">Contact Us</a></div>

        </div>
    </div>
@endsection