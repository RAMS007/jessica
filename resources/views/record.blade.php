@extends('layout.baseBootstrap')
@section('content')

    <script src='//cameratag.com/api/v9/js/cameratag.min.js' type='text/javascript'></script>
    <link rel='stylesheet' href='//cameratag.com/static/9/cameratag.css'>
    <h1>Step 2: Recording</h1>


    <div class="row">
        <div class="col-sm-6">
            <div class="scriptReview center">
                <p>
                    <b>{{$input['introducion'] or '' }}</b>
                </p>

                <p>
                    {{$input['topic1'] or '' }}
                </p>

                <p>
                    <b>{{$input['topic2'] or '' }}</b>
                </p>
            </div>
        </div>
        <div class="col-sm-6">
            <div>
                <div class="s10imgpreloader" id="comp-iu5lk21oimgpreloader"></div>
                <camera data-app-id='a-9a5e5660-75a3-0134-c71d-1217e1551dfd' id='myCamera'></camera>
            </div>
            <div class="buttonsBlock">
                <form method="POST" action="{{url('/select-video')}}" style="display: inline-block;">
                    {{csrf_field()}}
                    <input type="hidden" name="introducion" value="{{$input['introducion'] or '' }}">
                    <input type="hidden" name="topic1" value="{{$input['topic1'] or '' }}">
                    <input type="hidden" name="topic2" value="{{$input['topic2'] or '' }}">
                    <input type="submit" class="redButton" value="Move ON">
                </form>
            </div>
        </div>
    </div>

    <div class="test">

    </div>

    <h2>Video Attempts </h2>
    <div id="attempts">
        <!--
                                    <div class="imageAttempts"><img  src="https://static.wixstatic.com/media/90b465_37270914bc6944acb23af899e26bcb1e~mv2.png/v1/fill/w_293,h_164,al_c,usm_0.66_1.00_0.01/90b465_37270914bc6944acb23af899e26bcb1e~mv2.png" ></div>
                                   <div class="imageAttempts"><img  src="https://static.wixstatic.com/media/90b465_37270914bc6944acb23af899e26bcb1e~mv2.png/v1/fill/w_293,h_164,al_c,usm_0.66_1.00_0.01/90b465_37270914bc6944acb23af899e26bcb1e~mv2.png" ></div>
        -->
    </div>
    <div class="clearfix"></div>
@endsection

@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            var timerId = setInterval(function () {
                $.getJSON("{{url('/')}}/preview", function (data) {
                    var items = '';
                    $.each(data, function (key, val) {
                        items = items + '<div class="imageAttempts"><img  src="/a-9a5e5660-75a3-0134-c71d-1217e1551dfd/' + val + '" ></div>';

                    });
                    $('#attempts').html(items);
                    console.log(items);
                });
            }, 2000);
        })
    </script>
@endsection