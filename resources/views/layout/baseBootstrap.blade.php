<!DOCTYPE html>
<html>
<head>
    <title>Jess</title>
    <link rel="stylesheet" href="{{url('/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/css/styles.css')}}">
    <base href="http://139.59.130.84">
</head>
<body>

<nav class="navbar  navbar-fixed-top">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="{{url('/')}}">Home</a></li>
            <!--      <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="{{url('/get-started')}}">Get started<span
                        class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/select-video')}}">Select video</a></li>
                    <li><a href="{{url('/final-page-review')}}">Final page/review</a></li>
                </ul>
            </li>-->
            <li><a href="{{url('/get-started')}}">Get started</a></li>
            <li><a href="{{url('/video-resumes')}}">Video Resumes</a></li>
            <li><a href="{{url('/our-process')}}">Our Process</a></li>
            <li><a href="{{url('/our-team-1')}}">Our Team</a></li>
            <li><a href="{{url('/support')}}">Support</a></li>
        </ul>
    </div>
</nav>
<div class="content">
    @yield('content')
</div>

<div class="footer">
    <div class="social">
        <a href="http://www.facebook.com/wix" target="_blank">
            <img alt="" src="{{url('/img/fb.png')}}">
        </a>
    </div>
    <div class="social">
        <a href="http://www.twitter.com/wix" target="_blank">
            <img alt="" src="{{url('/img/twiter.png')}}">
        </a>
    </div>
    <div class="social">
        <a href="https://www.linkedin.com/company/wix-com" target="_blank">
            <img alt="" src="{{url('/img/in.png')}}">
        </a>
    </div>
</div>
<script src="{{url('/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{url('/js/bootstrap.min.js')}}"></script>
@yield('script')
</body>
</html>