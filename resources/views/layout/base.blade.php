<html class="">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="utf-8">
    <title>Jess</title>
    <meta name="fb_admins_meta_tag" content="">
    <link rel="shortcut icon" href="http://www.wix.com/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="http://www.wix.com/favicon.ico" type="image/x-icon">
    <script src="{{asset('js/script_small.js')}}"></script>
    <link rel="stylesheet"
          href="{{asset('css/styles_small.css')}}">

    <base href="http://139.59.130.84">

</head>
<body class="">
<div id="SITE_CONTAINER">
    <div class="noop">
        <div class="SITE_ROOT" id="SITE_ROOT" style="width:980px;padding-bottom:40px;">
            <div id="masterPage" style="width: 980px; position: static; top: 0px; /*height: 3336px;*/">

                <div style="@yield('footerStyle')"
                     class="s0_footer s0" data-state=" " data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER">
                    <div class="s0screenWidthBackground" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.0"
                         style="width: 1007px; left: -14px;">
                        <div class="s0_bg" data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.0.0"></div>
                    </div>
                    <div id="SITE_FOOTERcenteredContent" class="s0centeredContent">
                        <div id="SITE_FOOTERbg" class="s0bg"
                             data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.0"></div>
                        <div id="SITE_FOOTERinlineContent" class="s0inlineContent"
                             data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1">
                            <div style="width: 150px; left: 432px; position: absolute; top: 32px; height: 30px;"
                                 class="s1" id="comp-ihjai4yz"
                                 data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz">
                                <div id="comp-ihjai4yzitemsContainer" class="s1itemsContainer"
                                     data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0">
                                    <div style="width:30px;height:30px;margin-bottom:0;margin-right:13px;display:inline-block;"
                                         class="s1imageItem" id="comp-ihjai4yzdataItem-ihjai4yz2"
                                         data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.0">
                                        <a href="http://www.facebook.com/wix" target="_blank"
                                           data-content="http://www.facebook.com/wix" data-type="external"
                                           id="comp-ihjai4yzdataItem-ihjai4yz2link" class="s1imageItemlink"
                                           data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.0.0">
                                            <div style="position:absolute;width:30px;height:30px;"
                                                 id="comp-ihjai4yzdataItem-ihjai4yz2image" class="s1imageItemimage"
                                                 data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.0.0.0">
                                                <div class="s1imageItemimagepreloader"
                                                     id="comp-ihjai4yzdataItem-ihjai4yz2imagepreloader"
                                                     data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.0.0.0.0"></div>
                                                <img id="comp-ihjai4yzdataItem-ihjai4yz2imageimage" alt=""
                                                     src="https://static.wixstatic.com/media/0fdef751204647a3bbd7eaa2827ed4f9.png/v1/fill/w_38,h_38,al_c,usm_0.66_1.00_0.01/0fdef751204647a3bbd7eaa2827ed4f9.png"
                                                     style="width:30px;height:30px;object-fit:cover;"
                                                     data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.0.0.0.$image">
                                            </div>
                                        </a></div>
                                    <div style="width:30px;height:30px;margin-bottom:0;margin-right:13px;display:inline-block;"
                                         class="s1imageItem" id="comp-ihjai4yzdataItem-ihjai4z01"
                                         data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.1">
                                        <a href="http://www.twitter.com/wix" target="_blank"
                                           data-content="http://www.twitter.com/wix" data-type="external"
                                           id="comp-ihjai4yzdataItem-ihjai4z01link" class="s1imageItemlink"
                                           data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.1.0">
                                            <div style="position:absolute;width:30px;height:30px;"
                                                 id="comp-ihjai4yzdataItem-ihjai4z01image" class="s1imageItemimage"
                                                 data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.1.0.0">
                                                <div class="s1imageItemimagepreloader"
                                                     id="comp-ihjai4yzdataItem-ihjai4z01imagepreloader"
                                                     data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.1.0.0.0"></div>
                                                <img id="comp-ihjai4yzdataItem-ihjai4z01imageimage" alt=""
                                                     src="https://static.wixstatic.com/media/c7d035ba85f6486680c2facedecdcf4d.png/v1/fill/w_38,h_38,al_c,usm_0.66_1.00_0.01/c7d035ba85f6486680c2facedecdcf4d.png"
                                                     style="width:30px;height:30px;object-fit:cover;"
                                                     data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.1.0.0.$image">
                                            </div>
                                        </a></div>
                                    <div style="width:30px;height:30px;margin-bottom:0;margin-right:13px;display:inline-block;"
                                         class="s1imageItem" id="comp-ihjai4yzdataItem-ihjak4aj"
                                         data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.2">
                                        <a href="https://www.linkedin.com/company/wix-com" target="_blank"
                                           data-content="https://www.linkedin.com/company/wix-com" data-type="external"
                                           id="comp-ihjai4yzdataItem-ihjak4ajlink" class="s1imageItemlink"
                                           data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.2.0">
                                            <div style="position:absolute;width:30px;height:30px;"
                                                 id="comp-ihjai4yzdataItem-ihjak4ajimage" class="s1imageItemimage"
                                                 data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.2.0.0">
                                                <div class="s1imageItemimagepreloader"
                                                     id="comp-ihjai4yzdataItem-ihjak4ajimagepreloader"
                                                     data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.2.0.0.0"></div>
                                                <img id="comp-ihjai4yzdataItem-ihjak4ajimageimage" alt=""
                                                     src="https://static.wixstatic.com/media/6ea5b4a88f0b4f91945b40499aa0af00.png/v1/fill/w_38,h_38,al_c,usm_0.66_1.00_0.01/6ea5b4a88f0b4f91945b40499aa0af00.png"
                                                     style="width:30px;height:30px;object-fit:cover;"
                                                     data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjai4yz.0.2.0.0.$image">
                                            </div>
                                        </a></div>
                                </div>
                            </div>
                            <div style="left: 38px; width: 307px; position: absolute; top: 36px;" class="s2"
                                 id="comp-ihjalf3l"
                                 data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjalf3l"><p
                                        class="font_8"> by The Axis Group</p>
                            </div>
                            <div style="left: 631px; width: 305px; position: absolute; top: 36px;" class="s2"
                                 id="comp-ihjaml41"
                                 data-reactid=".0.$SITE_ROOT.$desktop_siteRoot.$SITE_FOOTER.1.1.$comp-ihjaml41">
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 980px; position: fixed; z-index: 50; top: 0px; height: 64px; left: 0px;" class="s3"
                     data-state=" fixedPosition" id="SITE_HEADER">
                    <div id="SITE_HEADERcenteredContent" class="s3centeredContent">
                        <div id="SITE_HEADERbg" class="s3bg"></div>
                        <div id="SITE_HEADERinlineContent" class="s3inlineContent">
                            <ul class="dropdown">
                                <li class="dropdown-top">
                                    <a class="dropdown-top" href="{{url('/')}}">Home</a>
                                </li>

                                <li class="dropdown-top">
                                    <a class="dropdown-top" href="{{url('/get-started')}}">Get started</a>
                                    <ul class="dropdown-inside">
                                        <!--             <li><a href="{{url('/prepare')}}">Prepare</a></li>
                                      <li><a href="{{url('/review-script')}}">Review script</a></li>
                                        <li><a href="{{url('/record')}}">Record</a></li> -->
                                        <li><a href="{{url('/select-video')}}">Select video</a></li>
                                  <!--      <li><a href="{{url('/edit')}}">Edit</a></li> -->
                                        <li><a href="{{url('/final-page-review')}}">Final page/review</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown-top">
                                    <a class="dropdown-top" href="{{url('/video-resumes')}}">Video Resumes</a>
                                </li>

                                <li class="dropdown-top">
                                    <a class="dropdown-top" href="{{url('/our-process')}}">Our Process</a>
                                </li>

                                <li class="dropdown-top">
                                    <a class="dropdown-top" href="{{url('/our-team-1')}}">Our Team</a>
                                    <ul class="dropdown-inside">
                                        <li><a href="{{url('/clients')}}">Clients</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-top">
                                    <a class="dropdown-top" href="{{url('/support')}}">Support</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div style="width: 980px; position: absolute; top: 64px; /*height: 3112px;*/ left: 0px;" class="s5"
                     id="PAGES_CONTAINER">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
@yield('script')
</body>
</html>