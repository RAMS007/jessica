@extends('layout.baseBootstrap')

@section('content')

    <h1>Review Script</h1>


    <div class ="scriptReview center">
        <p>
            <b>{{$input['introducion'] or '' }}</b>
        </p>

        <p>
            {{$input['topic1'] or '' }}
        </p>

        <p>
            <b>{{$input['topic2'] or '' }}</b>
        </p>
    </div>

    <div class ="buttonsBlock center">
        <form method="POST" action="{{url('/prepare')}}">
            {{csrf_field()}}
            <input type="hidden" name="introducion" value="{{$input['introducion'] or '' }}">
            <input type="hidden" name="topic1" value="{{$input['topic1'] or '' }}">
            <input type="hidden" name="topic2" value="{{$input['topic2'] or '' }}">
            <input type="submit" class="redButton" value="EDIT SCRIPT">
        </form>
            <form method="POST" action="{{url('/record')}}">
                {{csrf_field()}}
                <input type="hidden" name="introducion" value="{{$input['introducion'] or '' }}">
                <input type="hidden" name="topic1" value="{{$input['topic1'] or '' }}">
                <input type="hidden" name="topic2" value="{{$input['topic2'] or '' }}">

                <input type="submit" class="redButton" value="MOVE ON"
                     >

            </form>
  </div>
@endsection