@extends('layout.baseBootstrap')
@section('content')
    <div id="background">
        <h2>VIDEO RESUMES </h2>
    </div>

    <div class="wrapper">
        <div class="row">
            <div class="col-sm-6">
                <h1>What are Video Resumes?</h1>

                <div><p>Video resumes are a way for candidates to go beyond
                     traditional methods of applying, such as submitting
                     only a resume, cover letter, and work samples. Lasting
                     typically 60 seconds, these videos are your shot to
                     make the best first impression to an employer. A video
                     resume lets the employer literally see you and hear
                     your case (via your communication skills, personality
                     and charisma) as the best candidate for the job - all
                     before the interview takes place.</p>
                </div>
            </div>
            <div class="col-sm-6">
                <img alt=""
                     src="{{url('/img/idea.png')}}">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <img alt=""
                     src="{{url('/img/movie.png')}}">
            </div>
            <div class="col-sm-6">
                <h1>How do I create one?</h1>

                <div><p>It’s <b>Easy</b> and <b>Free!!</b> Rezzio has created an easy to use program
                        giving you the ability to
                        create your video resume
                        with three simple steps.
                        Our Process breaks down the
                        video resume process into
                        three easy steps. Prepare.
                        Record. Edit. &nbsp;Prepare
                        your script. Record your
                        video. Edit your final
                        product.</p>
                </div>
            </div>
        </div>

        <h1>Our Examples</h1>

        <div class="row">
            <div class="col-sm-4">
                <iframe allowfullscreen=""
                        src="//www.youtube.com/embed/PMWmYf2043g?wmode=transparent&amp;autoplay=0&amp;theme=dark&amp;controls=1&amp;autohide=0&amp;loop=0&amp;showinfo=0&amp;rel=0&amp;playlist=false&amp;enablejsapi=0"
                        width="314" height="150" ></iframe>
                <div>
                    <h2>Richard Keshgegian,</h2>

                    <p>Desgin Construction Consultant for PECO Energy&nbsp;</p>
                </div>
            </div>
            <div class="col-sm-4">
                <iframe allowfullscreen=""
                        src="//www.youtube.com/embed/PMWmYf2043g?wmode=transparent&amp;autoplay=0&amp;theme=dark&amp;controls=1&amp;autohide=0&amp;loop=0&amp;showinfo=0&amp;rel=0&amp;playlist=false&amp;enablejsapi=0"
                        width="314" height="150" ></iframe>
                <div>
                    <h2>Whiney Keyes, </h2>

                    <p>Professional Dick Rider</p>
                </div>
            </div>

            <div class="col-sm-4">
                <iframe allowfullscreen=""
                        src="//www.youtube.com/embed/PMWmYf2043g?wmode=transparent&amp;autoplay=0&amp;theme=dark&amp;controls=1&amp;autohide=0&amp;loop=0&amp;showinfo=0&amp;rel=0&amp;playlist=false&amp;enablejsapi=0"
                        width="314" height="150" ></iframe>
                <div>
                    <h2>Claire MacDougall,</h2>

                    <p>Account Manager at TaxProAlliance </p>
                </div>
            </div>
        </div>
    </div>
@endsection