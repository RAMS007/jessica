@extends('layout.baseBootstrap')

@section('content')
    <div id="backgroundTeam">
        <h2>ABOUT </h2>
    </div>
    <div class="wrapper">
        <div>
            <h5>The Axis Group</h5>

            <h6>&nbsp;</h6>

            <p>I'm a paragraph. Click here to add your own text and edit
               me. It’s easy. Just click “Edit Text” or double click me
               to add your own content and make changes to the font. Feel
               free to drag and drop me anywhere you like on your page.
               I’m a great place for you to tell a story and let your
               users know a little more about you.</p>

            <p>This is a great space to write long text about your
               company and your services. You can use this space to go
               into a little more detail about your company. Talk about
               your team and what services you provide. Tell your
               visitors the story of how you came up with the idea for
               your business and what makes you different from your
               competitors. Make your company stand out and show your
               visitors who you are.​</p>
        </div>

        <h5>Our Team</h5>

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-4">
                        <img alt=""
                             src="{{url('/img/team1.jpg')}}">
                    </div>
                    <div class="col-sm-8">
                        <span class="sizeTop1">Jeff Florick,&nbsp;CEO</span>

                        <p><a href="mailto:Jeff@axisgroup.com">Jeff@axisgroup.com</a>
                            | Phone:&nbsp;123-456-7890</p>
                    </div>
                </div>
                <p>This is a great space to write long text about your
                   company and your services. You can use this space to go
                   into a little more detail about your company. Talk about
                   your team and what services you provide. Tell your
                   visitors the story of how you came up with the idea for
                   your business and what makes you different from your
                   competitors. Make your company stand out and show your
                   visitors who you are.</p>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-4">
                        <img alt=""
                             src="{{url('/img/team2.jpg')}}">
                    </div>
                    <div class="col-sm-8">
                        <span class="sizeTop1">Jenny Van Bingen, CFO</span>

                        <p><a href="mailto:Jenny@axisgroup.com">Jenny@axisgroup.com</a>
                            | Phone:&nbsp;123-456-7890</p>
                    </div>
                </div>
                <p>This is a great space to write long text about your
                   company and your services. You can use this space to go
                   into a little more detail about your company. Talk about
                   your team and what services you provide. Tell your
                   visitors the story of how you came up with the idea for
                   your business and what makes you different from your
                   competitors. Make your company stand out and show your
                   visitors who you are.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-4">
                        <img alt=""
                             src="{{url('/img/team3.jpg')}}">
                    </div>
                    <div class="col-sm-8">
                        <span class="sizeTop1">Nora Lee Smith, HR</span>

                        <p><a href="mailto:Noralee@axisgroup.com">Noralee@axisgroup.com</a>
                            | Phone:&nbsp;123-456-7890</p>
                    </div>
                </div>
                <p>This is a great space to write long text about your
                   company and your services. You can use this space to go
                   into a little more detail about your company. Talk about
                   your team and what services you provide. Tell your
                   visitors the story of how you came up with the idea for
                   your business and what makes you different from your
                   competitors. Make your company stand out and show your
                   visitors who you are.</p>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-4">
                        <img alt=""
                             src="{{url('/img/team4.jpg')}}">
                    </div>
                    <div class="col-sm-8">
                        <span class="sizeTop1">Denis G. Roberts,&nbsp;Sales Manager</span>

                        <p><a href="mailto:Denis@axisgroup.com">Denis@axisgroup.com</a>
                            | Phone:&nbsp;123-456-7890</p>
                    </div>
                </div>
                <p>This is a great space to write long text about your
                   company and your services. You can use this space to go
                   into a little more detail about your company. Talk about
                   your team and what services you provide. Tell your
                   visitors the story of how you came up with the idea for
                   your business and what makes you different from your
                   competitors. Make your company stand out and show your
                   visitors who you are.</p>
            </div>
        </div>

        <div>
            <h5><span>Need more details? Contact us</span>
            </h5>

            <p><span>We are here to assist. Contact us by phone, email or via our Social Media channels.</span>
            </p>

            <div>
                <a href="{{url('/support')}}" target="_self">Contact Us</a></div>

        </div>
    </div>
@endsection