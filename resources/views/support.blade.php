@extends('layout.baseBootstrap')

@section('content')

    <div id="backgroundSupport">
        <h2>CONTACT </h2>
    </div>
    <div class="wrapper">

        <div class="row">

            <div class="col-sm-6">
                <h5>Rezzio</h5>

                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email:</label>

                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Phone">Phone:</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="Phone" placeholder="Enter Phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Address">Address:</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="Address" placeholder="Enter Address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="Subject">Subject:</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="Subject" placeholder="Enter Subject">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="message">Message:</label>

                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" id="message"
                                      placeholder="Enter message"> </textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-sm-6">
                <iframe src="https://static.parastorage.com/services/santa/1.1800.14/static/external/googleMap.html?language=en"
                        scrolling="no" width="400px" height="200px" frameborder="0"></iframe>
                <div class="row">

                    <div class="col-sm-6">
                        <h6>Our Address</h6>
                        <h6>&nbsp;</h6>

                        <p>3230 Market Street-&nbsp;Suite 401</p>

                        <p>Philadelphia, PA&nbsp; 19104</p>

                        <p><a href="mailto:info@mysite.com">info@mysite.com</a>
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <h6>Call Us</h6>
                        <h6></h6>

                        <p>Tel: 856-577-2033 </p>

                        <p>Fax: 123-456-7890 </p>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection