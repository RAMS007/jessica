@extends('layout.baseBootstrap')

@section('content')
    <form method="POST" action="{{url('/review-script')}}" class="form-horizontal">
        {{csrf_field()}}
        <h1>Step 1: Preparation</h1>

        <div class="error">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div>
            <p class="font7 text-center">Here you will write your script about what you would like to disucss.
                                         We
                                         have broken down the topics.&nbsp;</p>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="introducion">Introduction</label>
            <div class="col-sm-8">
               					<textarea
                                        name="introducion">@if(isset($input['introducion'])){{$input['introducion']}}@else{{ old('introducion') }}@endif</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="topic1">Topic 1</label>
            <div class="col-sm-8">
               <textarea
                       name="topic1">@if(isset($input['topic1'])){{$input['topic1']}}@else{{ old('topic1') }}@endif</textarea>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4" for="topic2">Topic 2</label>
            <div class="col-sm-8">
                <textarea
                        name="topic2">@if(isset($input['topic2'])){{$input['topic2']}}@else{{ old('topic2') }}@endif</textarea>

            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-6 col-sm-10">
                <button type="submit" class="redButton">CREATE SCRIPT</button>
            </div>
        </div>
    </form>
@endsection